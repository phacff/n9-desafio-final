from main import Moradia, Apartamento

def test_casa():
    result = Moradia(100, 'Rua das Limeiras', 672, 250000)
    expected = {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 250000, 'tipo': 'Não especificado'}
    assert result.__dict__ == expected


def test_relatorio_de_ppreco():
    casa = Moradia(100, 'Rua das Limeiras', 672, 250000)
    result = casa.gerar_relatorio_preco()
    expected = 2500
    assert result == expected


def test_apt():
    apt = Apartamento(100, 'Rua das Limeiras', 672, 500000, 110, 700, 2)
    expected = {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 500000, 'tipo': 'Apartamento', 'preco_condominio': 700, 'andar': 11, 'num_apt': 110, 'num_elevadores': 2}
    assert apt.__dict__ == expected


def test_relatorio_apt():
    apt = Apartamento(100, 'Rua das Limeiras', 672, 500000, 110, 700, 2)
    result = apt.gerar_relatorio()
    expected = 'Rua das Limeiras - apt 110 - andar: 11 - elevadores: 2 - preco por m²: R$ 5000'
    assert result == expected
